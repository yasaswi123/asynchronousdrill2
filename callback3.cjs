function cardIdInformation(listId,cards,callback){
    try{
        const cardInformation = Object.entries(cards).filter(current => current[0] == listId)
        setTimeout(()=>{
            callback(null,Object.fromEntries(cardInformation))
        },2*1000)
    }
    catch(error){
        console.error(error)
    }
}

module.exports = cardIdInformation