function listsidInformation(boardId,lists,callback){
    try{
        const list = Object.entries(lists).filter(current => current[0] == boardId)
        setTimeout(()=>{
            callback(null,Object.fromEntries(list))
        },2*1000)
    }
    catch(error){
        console.error(error)
    }
}

module.exports = listsidInformation