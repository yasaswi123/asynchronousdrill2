const boardsInformation = require('./callback1.cjs')
const listsInformation = require('./callback2.cjs')
const cardsInformation = require('./callback3.cjs')
const boards = require('./data/boards.json')
const lists = require('./data/lists.json')
const cards= require('./data/cards.json')


function thanosMindlist(personName){
    try{
        const identity = boards.filter(list => list.name == personName)
        boardsInformation(identity[0].id,boards,(err1,data1)=>{
            if(err1){
                console.error(err1)
            }
            else{
                console.log(data1)
                console.log("board id information successful")
                listsInformation(data1[0].id,lists,(err2,data2)=>{
                    if(err2){
                        console.log(err2)
                    }
                    else{
                        console.log(data2)
                        console.log("list information successful")
                        const convert = Object.entries(data2)
                        const mindInformation = convert[0][1].filter(current=>current.name == "Mind")
                        cardsInformation(mindInformation[0].id,cards,(err3,data3)=>{
                            if(err3){
                                console.error(err)
                            }
                            else{
                                console.log(data3)
                                console.log("mind information of cards")
                            }
                        })
                    }
                })
            }
        })
    }
    catch(error){
        console.error(error)
    }
}

module.exports = thanosMindlist
